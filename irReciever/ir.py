#!/usr/bin/env python
import RPi.GPIO as GPIO

IrPin = 11
count = 0


def setup():
    GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location
    GPIO.setup(IrPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def cnt(ev):
    print('Edge detected on channel %s' % ev)
    global count
    count += 1
    print('Received infrared {}, cnt = {}'.format(ev, count))


def loop():

    GPIO.add_event_detect(IrPin, GPIO.BOTH, callback=cnt) # wait for falling
    while True:
        pass   # Don't do anything


def destroy():
    GPIO.cleanup()                     # Release resource


if __name__ == '__main__':     # Program start from here
    setup()
    try:
        loop()
    except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        destroy()
